import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Md5 } from 'ts-md5/dist/md5';
import { environment } from 'src/environments/environment';

export class MarvelInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if(req.url.includes(environment.marvelApi)){
      let t = (new Date).getTime().toString();
      let hash = (new Md5).appendStr(t + environment.priKey + environment.pubKey).end().toString();
      const newRequest = req.clone({setParams: {
        'ts': t,
        'apikey': environment.pubKey,
        'hash': hash
      }});
      return next.handle(newRequest);
    }
    return next.handle(req);
  }
}
