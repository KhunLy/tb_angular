import { LoaderService } from './_services/loader.service';
import { MarvelInterceptor } from './_interceptors/marvel.interceptor';
import { CharacterService } from './_services/character.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbInputModule, NbCardModule, NbListModule, NbIconModule, NbDialogModule, NbDialogConfig } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { Ex1Component } from './components/ex1/ex1.component';
import { Ex2Component } from './components/ex2/ex2.component';
import { FormsModule } from '@angular/forms';
import { Ex2ConfirmDialogComponent } from './components/ex2-confirm-dialog/ex2-confirm-dialog.component';
import { Ex3Component } from './components/ex3/ex3.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderInterceptor } from './_interceptors/loader.interceptor';
import { Ex3detailsComponent } from './components/ex3details/ex3details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    AboutComponent,
    Ex1Component,
    Ex2Component,
    Ex2ConfirmDialogComponent,
    Ex3Component,
    LoaderComponent,
    Ex3detailsComponent
  ],
  entryComponents: [
    Ex2ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbButtonModule,
    NbInputModule,
    FormsModule,
    NbCardModule,
    NbListModule,
    NbIconModule,
    NbDialogModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    CharacterService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MarvelInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
