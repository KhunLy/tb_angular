import { PokemonType } from './../../_models/pokemon.type';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PokeTypeService } from '../../_services/poke-type.service';

@Component({
  selector: 'app-demo9',
  templateUrl: './demo9.component.html',
  styleUrls: ['./demo9.component.scss']
})
export class Demo9Component implements OnInit {



  private _typeForm : FormGroup;
  public get typeForm() : FormGroup {
    return this._typeForm;
  }
  public set typeForm(v : FormGroup) {
    this._typeForm = v;
  }


  constructor(private pokeTypeService: PokeTypeService) {
    this.typeForm = new FormGroup({
      Name: new FormControl(null, { validators: [
        Validators.required,
        Validators.maxLength(50)
      ] })
    });
  }

  ngOnInit() {
  }

  onSubmit(type: PokemonType) {
    this.pokeTypeService.create(type)
      .subscribe(data => {
        // afficher un message de success
      }, error => {
        //afficher un message d'erreur
      });
  }

}
