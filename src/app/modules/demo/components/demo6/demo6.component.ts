import { Component, OnInit } from '@angular/core';
import { Product } from '../../_models/product';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {

  
  private _products : Product[];
  public get products() : Product[] {
    return this._products;
  }
  public set products(v : Product[]) {
    this._products = v;
  }
  
  constructor() { 
    this.products = [
      { id:1, name: 'Coca Cola', price: 1, discount: 20, image: 'coca.jpg'},
      { id:2, name: 'Fanta', price: 0.9, discount: null, image: 'fanta.jpg'},
      { id:3, name: 'Sprite', price: 0.9, discount: 10, image: 'sprite.jpg'},
      { id:4, name: 'Dr Pepper', price: 1.1, discount: null, image: 'pepper.jpg'},
      { id:5, name: 'Nalu', price: 1.2, discount: 11.5, image: 'nalu.jpg'},
    ];
    console.log(this.products);
  }

  ngOnInit() {
  }

}
