import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  
  private _open : boolean;
  public get open() : boolean {
    return this._open;
  }
  public set open(v : boolean) {
    this._open = v;
  }
  
  constructor() { }

  ngOnInit() {
  }

  toggle(){
    this.open = !this.open;
  }

}
