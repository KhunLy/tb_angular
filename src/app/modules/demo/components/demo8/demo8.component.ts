import { Component, OnInit } from '@angular/core';
import { MeteoService } from '../../_services/meteo.service';
import { Meteo } from '../../_models/meteo';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component implements OnInit {

  private _meteo : Meteo;
  public get meteo() : Meteo {
    return this._meteo;
  }
  public set meteo(v : Meteo) {
    this._meteo = v;
  }

  constructor(private meteoService: MeteoService) { }

  ngOnInit() {
    this.meteoService.getMeteo(50.274,5.062)
    .subscribe(data => {
      this.meteo = data;
    });
  }

}


