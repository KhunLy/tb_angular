import { PokemonType } from './../_models/pokemon.type';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PokeTypeService {

  constructor(private client: HttpClient) { }

  create(type: PokemonType) : Observable<number>{
    return this.client
      .post<number>('http://localhost:50596/api/type', type)
  }
}
