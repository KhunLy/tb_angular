import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Meteo } from '../_models/meteo';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {

  private _endPoint: string = "https://api.openweathermap.org/data/2.5/weather?lat=__lat__&lon=__lng__&APPID=__key__&units=metric";
  //private _client: HttpClient

  // rem: NE PAS PRENDRE LE HTTPCLIENT DE SELENIUM!!!!!
  constructor(private client: HttpClient) 
  { 
    //this._client = client;
  }

  getMeteo(lat: number, lng: number) : Observable<Meteo> {
    let request = this._endPoint
      .replace('__lat__', lat.toString())
      .replace('__lng__', lng.toString())
      .replace('__key__', environment.meteoKey);
    return this.client.get<Meteo>(request);
  }
}
