import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  private _isLoading : boolean;
  public get isLoading() : boolean {
    return this._isLoading;
  }
  public set isLoading(v : boolean) {
    this._isLoading = v;
  }

  constructor(private loaderService: LoaderService) {

  }

  ngOnInit() {
      this.loaderService.isLoading.subscribe(data => this.isLoading = data);
  }

}
