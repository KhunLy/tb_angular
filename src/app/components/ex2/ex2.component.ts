import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/_models/article';
import { NbDialogService } from '@nebular/theme';
import { Ex2ConfirmDialogComponent } from '../ex2-confirm-dialog/ex2-confirm-dialog.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  private obs: Observable<number>;

  private input: string;

  private articles: Article[];

	public get $input(): string {
		return this.input;
	}

	public set $input(value: string) {
		this.input = value;
  }
  
	public get $articles(): Article[] {
		return this.articles;
	}

	public set $articles(value: Article[]) {
		this.articles = value;
	}

  constructor(private dialogService: NbDialogService) {
    this.$articles = [];
  }

  ngOnInit() {
  }

  add(){
    if(this.$input != null && this.$input != ''){
      let item = this.$articles.find(a => a.name == this.$input.trim().toLowerCase());
      if(item == null){
        this.$articles.push({
          name: this.$input.trim().toLowerCase(), 
          isChecked: false, 
          quantity: 1
        });
      }
      else{
        item.quantity++;
      }
      this.$input = null;
    }
  }

  onKeyUp(e: KeyboardEvent){
    if(e.key == 'Enter'){
      this.add();
    }
  }

  checked(item: Article){
    item.isChecked = !item.isChecked;
  }

  openConfirmDialog(item: Article){
    //ouvrir une fenêtre de dialog

    let dialodRef = this.dialogService.open(
      Ex2ConfirmDialogComponent, { closeOnBackdropClick: true  }
    );
    dialodRef.onClose.subscribe((data) => {
      if(data){
        //supprimer
        this.$articles.splice(this.$articles.indexOf(item), 1);
      }
    });
  }

}
