import { Result } from './../../_models/characterRequest';
import { CharacterService } from './../../_services/character.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterRequest } from 'src/app/_models/characterRequest';

@Component({
  selector: 'app-ex3details',
  templateUrl: './ex3details.component.html',
  styleUrls: ['./ex3details.component.scss']
})
export class Ex3detailsComponent implements OnInit {


  private _context : CharacterRequest;
  public get context() : CharacterRequest {
    return this._context;
  }
  public set context(v : CharacterRequest) {
    this._context = v;
  }

  public get hero(): Result {
    if(this.context != null)
      return this.context.data.results[0];
    return null;
  }


  constructor(
    private router: ActivatedRoute,
    private charService: CharacterService
  ) {
    let id = parseInt(this.router.snapshot.paramMap.get('id'));
    this.charService.get(id).subscribe(data => {
      this.context = data;
    });
  }

  ngOnInit() {
  }

}
