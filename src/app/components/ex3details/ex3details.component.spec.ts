import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ex3detailsComponent } from './ex3details.component';

describe('Ex3detailsComponent', () => {
  let component: Ex3detailsComponent;
  let fixture: ComponentFixture<Ex3detailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ex3detailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ex3detailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
