import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit  {

  private isStarted: boolean;

	public get $isStarted(): boolean {
		return this.isStarted;
	}

	public set $isStarted(value: boolean) {
		this.isStarted = value;
	}


  private timer: any;
  // implicitement le modificateur d'acces des variables est private
  private maVariable: number;

  public get MaVariable(){
    return this.maVariable;
  }

  public set MaVariable(value: number){
    if(value < 0) return;
    this.maVariable = value;
  }

  constructor() { 
    // le 'this' est obligatoire en TS
    this.MaVariable = 0;
    this.$isStarted = false;
  }

  //par default public
  public ngOnInit() {
    
  }

  start(){
    this.timer = setInterval(() => {
      this.MaVariable++
    }, 200);
    this.$isStarted = true;
  }

  stop(){
    clearInterval(this.timer);
    this.$isStarted = false;
  }

  reset(){
    this.maVariable = 0;
    this.stop();
  }
}
