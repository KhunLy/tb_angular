import { CharactersRequest } from './../../_models/charactersRequest';
import { CharacterService } from './../../_services/character.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex3',
  templateUrl: './ex3.component.html',
  styleUrls: ['./ex3.component.scss']
})
export class Ex3Component implements OnInit {


  private _context : CharactersRequest;
  public get context() : CharactersRequest {
    return this._context;
  }
  public set context(v : CharactersRequest) {
    this._context = v;
  }


  constructor(private charServ: CharacterService) { }

  ngOnInit() {
    this.charServ.getAll()
      .subscribe(data => this.context = data);
  }

}
