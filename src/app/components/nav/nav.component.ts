import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  items: NbMenuItem[] = [
    { title: 'Home', icon: 'home', link: '/home' },
    { title: 'About', icon: 'eye-outline', link: '/about' },
    { title: 'Exercices', icon: 'star', children: [
      { title: 'Ex1', link: '/ex1' },
      { title: 'Ex2', link: '/ex2' },
      { title: 'Ex3', link: '/ex3' },
    ] },
    { title: 'Demo', icon: 'sun', children: [
      { title: 'Demo 1 - Binding 1 Way', link: '/demo/demo1' },
      { title: 'Demo 2 - Events', link: '/demo/demo2' },
      { title: 'Demo 3 - Binding 2 Ways', link: '/demo/demo3' },
      { title: 'Demo 4 - *ngIf', link: '/demo/demo4' },
      { title: 'Demo 5 - *ngFor', link: '/demo/demo5' },
      { title: 'Demo 6 - Models', link: '/demo/demo6' },
      { title: 'Demo 7 - Observable', link: '/demo/demo7' },
      { title: 'Demo 8 - Meteo', link: '/demo/demo8' },
      { title: 'Demo 9 - Model driven Form', link: '/demo/demo9' },
    ] },

  ]

  constructor() { }

  ngOnInit() {
  }

}
