import { CharacterRequest } from './../_models/characterRequest';
import { CharactersRequest } from './../_models/charactersRequest';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  private _endPoint : string
    = `${environment.marvelApi}characters`;

  constructor(private client: HttpClient) { }

  getAll(offset: number = 0): Observable<CharactersRequest> {
    let reqUrl = `${this._endPoint}?offset=${offset}`;
    return this.client.get<CharactersRequest>(reqUrl);
  }

  get(id: number): Observable<CharacterRequest> {
    let reqUrl = `${this._endPoint}/` + id;
    return this.client.get<CharacterRequest>(reqUrl);
  }
}
